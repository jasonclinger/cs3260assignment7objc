//
//  ViewController.h
//  Assignment7
//
//  Created by Jason Clinger on 2/24/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WonderOfTheWorldObject.h"

@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    NSMutableArray* arrayOfSevenWonders;
    
    UICollectionView* cView;
}


@property (weak, nonatomic) IBOutlet UIImageView *imageview;


@end


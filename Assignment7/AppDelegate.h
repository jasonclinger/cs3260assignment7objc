//
//  AppDelegate.h
//  Assignment7
//
//  Created by Jason Clinger on 2/24/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


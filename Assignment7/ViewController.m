//
//  ViewController.m
//  Assignment7
//
//  Created by Jason Clinger on 2/24/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


typedef void(^myCompletion)(NSArray* data);

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    cView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [cView setDataSource:self];
    [cView setDelegate:self];
    
    [cView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [cView setBackgroundColor:[UIColor blueColor]];
    
    [self.view addSubview:cView];
    
    
    arrayOfSevenWonders = [NSMutableArray new];
    
    [self getSevenWondersInfoWithResponseBlock:^(NSArray *array) {
        
        for (NSDictionary* d in array) {
            WonderOfTheWorldObject* wotwo = [WonderOfTheWorldObject new];
            wotwo.image = d[@"image"];
            wotwo.image_thumb = d[@"image_thumb"];
            wotwo.name = d[@"name"];
            wotwo.location = d[@"location"];
            wotwo.region = d[@"region"];
            wotwo.wikipedia = d[@"wikipedia"];
            wotwo.year_built = d[@"year_built"];
            //wotwo.coord = d[@"latitude"];
            
            
            
            [arrayOfSevenWonders addObject:wotwo];
            
            
            NSURL *url = [NSURL URLWithString:wotwo.image_thumb];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            
            
            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            [[session dataTaskWithRequest:request
                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //Your main thread code goes in here
                                self.imageview.image = [UIImage imageWithData:data];
                                
                                
                            });
                            
                            
                            
                        }] resume];
        }
        
    }];
    
}




-(void)getSevenWondersInfoWithResponseBlock:(myCompletion) completion {
    NSString *urlString = [NSString stringWithFormat: @"http://aasquaredapps.com/Class/sevenwonders.json"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    // do stuff
                    completion([NSJSONSerialization JSONObjectWithData:data options:0 error:&error]);
                    
                }] resume];
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 7;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    
    //self.imageview.image = [UIImage imageNamed:]
    //getSevenWondersInfoWithResponseBlock(_imageview.image);
    
    //cell.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed: arrayOfSevenWonders. objectAtIndexPath.row]];
    cell.backgroundColor = [UIColor lightGrayColor];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.bounds.size.width/2 - 5, 250);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"here");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

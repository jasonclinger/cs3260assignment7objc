//
//  WonderOfTheWorldObject.h
//  Assignment7
//
//  Created by Jason Clinger on 2/24/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface WonderOfTheWorldObject : NSObject

@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* image_thumb;
@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* region;
@property (nonatomic, strong) NSString* wikipedia;
@property (nonatomic, strong) NSString* year_built;

@property (nonatomic, assign) CLLocationCoordinate2D coord;



@end
